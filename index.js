var express = require('express');
var ParseServer = require('parse-server').ParseServer;
var path = require('path');
var ParseDashboard = require('parse-dashboard');

var api = new ParseServer({
    databaseURI: 'mongodb://localhost:27017/dev',
    serverURL: 'http://ec2-13-232-98-71.ap-south-1.compute.amazonaws.com/parse',
    cloud: __dirname + '/cloud/main.js',
    appId: 'myAppId',
    masterKey: 'myMasterKey',
    clientKey: 'myClientKey',
    restAPIKey: 'myRestKey',
    allowClientClassCreation: true,
    liveQuery: {
        classNames: ["Posts", "Comments"] // List of classes to support for query subscriptions
    },
    verbose: false
});


var app = express();

var config = {
    "allowInsecureHTTP": true,
    "apps": [
        {
            "serverURL": "http://ec2-13-232-98-71.ap-south-1.compute.amazonaws.com/parse",
            "appId": "myAppId",
            "masterKey": "myMasterKey",
            "appName": "myApp"
        }
    ], "users": [
        {
            "user": "user",
            "pass": "pass"
        }
    ]
};

//var dashboard = new ParseDashboard(config, config.allowInsecureHTTP);
var dashboard = new ParseDashboard(config, {allowInsecureHTTP: config.allowInsecureHTTP});
// Serve static assets from the /public folder
app.use('/public', express.static(path.join(__dirname, '/public')));

// Serve the Parse API on the /parse URL prefix
var mountPath = process.env.PARSE_MOUNT || '/parse';
app.use(mountPath, api);
app.use('/dashboard', dashboard);
// Parse Server plays nicely with the rest of your web routes
app.get('/', function (req, res) {
    res.status(200).send('I dream of being a website.  Please star the parse-server repo on GitHub!');
});

// There will be a test page available on the /test path of your server url
// Remove this before launching your app
app.get('/test', function (req, res) {
    res.sendFile(path.join(__dirname, '/public/test.html'));
});

var port = 80;
var httpServer = require('http').createServer(app);
httpServer.listen(port, function () {
    console.log('MoveHack Server running on port ' + port + '.');
});

// This will enable the Live Query real-time server
ParseServer.createLiveQueryServer(httpServer);
