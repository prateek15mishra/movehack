import { Component, OnInit } from '@angular/core';

import {WindowRefService} from "../WindowRef/window-ref.service";
import {WalletService} from "../wallet/wallet.service";
import {SessionManagerService} from "../SessionManager/session-manager.service";
import {Location} from "@angular/common";
import { Route } from '../home/TravelOptions';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pay',
  templateUrl: './pay.component.html',
  styleUrls: ['./pay.component.css']
})
export class PayComponent implements OnInit {

  amount: string;
  walletBalance: string;
  aadhaarNo: string;
  qrData: string;

  constructor(private windowRefService: WindowRefService,
              private walletService: WalletService,
              private route: Router,
              private sessionServiceManager: SessionManagerService,
              private location: Location) {
  }

  rzp1: any;
  paymentReq: any;
  options: any = {
    "key": "rzp_test_BPh9OrHFTRwPqt",
    "amount": "0",
    "name": "MoveHack"
  };


  public initPay(amt): void {
    amt = parseFloat(amt) * 100;
    this.options.handler = ((response) => {
      this.paymentReq = {
        "paymentId": response.razorpay_payment_id,
        "amount": amt,
        "aadhaarNo": this.aadhaarNo
      };
      this.walletService.capturePayment(this.paymentReq)
        .then((res) => {
          if (res.result == 200) {
            location.reload();
            //TODO: Show in UI
            alert("Amount Added successfully!");
          }
        });
    });

    this.options.amount = amt;
    this.rzp1 = new this.windowRefService.nativeWindow.Razorpay(this.options);
    this.rzp1.open();
  }

  ngOnInit() {
    if (this.sessionServiceManager.getUserSessionToken() === null || this.sessionServiceManager.getUserSessionToken() === undefined) {
      this.route.navigateByUrl('/login');
    }

    this.amount = "0.00";
    this.aadhaarNo = this.sessionServiceManager.getAadhaar();
    
    this.qrData = this.aadhaarNo;
  }
}

