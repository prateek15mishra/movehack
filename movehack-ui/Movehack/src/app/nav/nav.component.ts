import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { SessionManagerService } from '../SessionManager/session-manager.service';
import { ServiceService } from '../service/service.service';
import { Subscription } from 'rxjs';
import { User } from "../user-transaction/UserDetails";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(private router: Router,
    public sessionServiceManager: SessionManagerService, private service: ServiceService) {
  }

  private subs: Subscription;
  private userDetailsSubscription: Subscription;


  public sessionToken: string;
  public loggedIn: boolean;
  public firstName: string = null;
  public lastName: string = "";
  public aadhaar: string = "";

  public logout() {
    this.sessionServiceManager.unsetUserSession();
    this.loggedIn = false;
    this.router.navigateByUrl('/login');
  }

  public loadPage(url) {
    console.log(url)
    switch (url) {
      case 'home':
        this.router.navigateByUrl('/home');
        break;

      case 'wallet':
        this.router.navigateByUrl('/wallet');
        break;

      case 'pay':
        this.router.navigateByUrl('/pay');
        break;

      case 'login':
        this.router.navigateByUrl('/login');
        break;

      case 'signup':
        this.router.navigateByUrl('/signup');
        break;

      case 'checkin':
        this.router.navigateByUrl('/pay');
        break;
    }
  }

  ngOnInit() {
    this.sessionToken = this.sessionServiceManager.getUserSessionToken();
    this.firstName = this.sessionServiceManager.getUserName();

    if (null === this.sessionToken || undefined === this.sessionToken) {
      this.loggedIn = false;
    } else {
      this.loggedIn = true;
    }

    this.subs = this.service.logMsg.subscribe(m => {
      this.loggedIn = m;
      console.log("from S", m);
    });


    this.userDetailsSubscription = this.service.firstName.subscribe(m => {
      this.firstName = m;
      this.sessionServiceManager.setUserFirstName(m);
    });

    this.userDetailsSubscription = this.service.lastName.subscribe(m => {
      this.lastName = m;
      this.sessionServiceManager.setUserLastName(m);
    });

    this.userDetailsSubscription = this.service.mobile.subscribe(m => {
      this.sessionServiceManager.setUserMobile(m);
    });

    this.userDetailsSubscription = this.service.aadhaar.subscribe(m => {
      this.sessionServiceManager.setAdhar(m);
    });

    this.userDetailsSubscription = this.service.email.subscribe(m => {
      this.sessionServiceManager.setEmail(m);
    });

    this.userDetailsSubscription = this.service.userType.subscribe(m => {
      this.sessionServiceManager.setUserType(m);
    });

    this.userDetailsSubscription = this.service.vendorType.subscribe(m => {
      this.sessionServiceManager.setVendorType(m);
    });

  };
}
