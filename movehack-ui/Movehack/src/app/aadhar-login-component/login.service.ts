import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from "@angular/common/http";
import {Observable} from "rxjs/index";

// let headers =  {
//   headers: new  HttpHeaders({
//     "X-Parse-Application-Id": "myAppId",
//     "Content-Type": "application/json",
//     "X-Parse-REST-API-Key": "myRestKey"
//   })
// };
//
// let options = new RequestOptions({ headers: headers });




@Injectable({
  providedIn: 'root'
})
export class LoginService {

  baseUri: string;
  constructor(private http: HttpClient) {
      this.baseUri = "http://ec2-13-232-98-71.ap-south-1.compute.amazonaws.com/parse/"
  }

  public tryLoggingIn(aadharNo): Promise<any> {
    const httpOptions = {
        headers: new HttpHeaders({
          "X-Parse-Application-Id": "myAppId",
          "Content-Type": "application/json",
          "X-Parse-REST-API-Key": "myRestKey"
        })
      };
      var post_body = {
          "aadhar" : aadharNo
      }
        return this.http.post(this.baseUri+ "functions/aadhar_auth",post_body,httpOptions)
            .toPromise()
            .then(res => res)
            .catch(this.handleError);
    }
  public verifyCode(code, number, aadhar): Promise<any> {
    const headers = {headers : new HttpHeaders()
        .set('X-Parse-Application-Id', "myAppId")
        .set('Content-Type', "application/json")
        .set("X-Parse-REST-API-Key","myRestKey")};
        var post_body = {
            "code": code,
            "number": number,
            "aadhar":aadhar
        }
        return this.http.post(this.baseUri+"functions/verify",post_body,headers)
        .toPromise()
        .then(res => res);
  }

  public getUserDetails(aadhar):Promise<any> {
    const headers = {headers : new HttpHeaders()
      .set('X-Parse-Application-Id', "myAppId")
      .set('Content-Type', "application/json")
      .set("X-Parse-REST-API-Key","myRestKey")};

      var post_body = {
        "aadhar": aadhar
      }
      return this.http.post(this.baseUri+"functions/getDetails",post_body,headers)
        .toPromise()
        .then(res => res);
  }

  public signup(fname,lname, aadhar, phone, email, userType, vendorType): Promise<any> {
      const headers = {headers : new HttpHeaders()
        .set('X-Parse-Application-Id', "myAppId")
        .set('Content-Type', "application/json")
        .set("X-Parse-REST-API-Key","myRestKey")};

        var post_body = {
            "fname": fname,
            "lname": lname,
            "aadhar": aadhar,
            "phoneNumber": phone,
            "email":email,
            "userType":userType,
            "vendorType":vendorType
        };

        return this.http.post(this.baseUri+"functions/signup", post_body, headers)
                .toPromise()
                .then(res => res);
  }

  private handleError(error): Promise<any> {
    console.log('Error From Service: ' + error);
    return Promise.reject(error.message || error);
  }

  public getDetails(): Promise<any> {
    const headers = {headers : new HttpHeaders()
      .set('X-Parse-Application-Id', "myAppId")
      .set('Content-Type', "application/json")
      .set("X-Parse-REST-API-Key","myRestKey")};

    return this.http.post(this.baseUri+"functions/getCurrent",{} ,headers)
            .toPromise()
            .then(res=>res);
  }
}

export class ResponseObj{
    results: any;
}
