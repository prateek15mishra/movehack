import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {LoginService} from './login.service';
import {Router} from '@angular/router';
import {SessionManagerService} from "../SessionManager/session-manager.service";
import { NavComponent } from '../nav/nav.component';
import { ServiceService } from '../service/service.service';

@Component({
  selector: 'login-component',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loggedIn: boolean;
  moveToAuth: boolean;

  aadhar: string;
  code: string;
  phoneNumber: string;

  constructor(private loginService: LoginService,
              private router: Router,
              private service:ServiceService,
              private sessionManagerService: SessionManagerService) {
    this.loggedIn = false;
    this.moveToAuth = false;
  }

  tryLoggingIn() {
    console.log(this.aadhar);
    this.loginService.tryLoggingIn(this.aadhar).then((res) => {
      this.phoneNumber = res.result;
      console.log(this.phoneNumber);
    });
    this.moveToAuth = true;
  }

  verifyCode() {
    console.log(this.code);
    this.loginService.verifyCode(this.code, this.phoneNumber, this.aadhar).then(((res) => {
      if (res.result === 401) {
        //TODO: Show in UI
        alert("Oops! OTP entered is incorrect");
        this.setLoggedIn(false);
      } else {
        
        this.sessionManagerService.setUserSession(res.result);
        this.sessionManagerService.getGetUserDetails(res.result);

        this.setLoggedIn(true);
        this.router.navigateByUrl("/home");
        this.setLoggedIn(true);
      }
    }));
  }

  public setLoggedIn(value: boolean) {
    this.service.setLoggedIn(value);
  }

  ngOnInit() {
  }

}
