import {Injectable, OnInit} from '@angular/core';
import {HttpHeaders, HttpClient} from "@angular/common/http";
import {ServiceService} from "../service/service.service";
import {SessionManagerService} from "../SessionManager/session-manager.service";

const httpOptions = {
  headers: new HttpHeaders({
    "X-Parse-Application-Id": "myAppId",
    "Content-Type": "application/json",
    "X-Parse-REST-API-Key": "myRestKey"
  })
};

@Injectable({
  providedIn: 'root'
})
export class WalletService implements OnInit {

  aadhaarNo: string;
  userType: string;
  resArr: Array<any>;

  constructor(private http: HttpClient,
              private serviceService: ServiceService,
              private sessionManagerService: SessionManagerService) {
  }

  public capturePayment(paymentReq): Promise<any> {
    return this.http
      .post('http://ec2-13-232-98-71.ap-south-1.compute.amazonaws.com/parse/functions/capturePayment', paymentReq, httpOptions)
      .toPromise()
      .then(response => response)
      .catch(this.handleError);
  }

  public getWalletBalance(req): Promise<any> {
    return this.http
      .post('http://ec2-13-232-98-71.ap-south-1.compute.amazonaws.com/parse/functions/getWalletBalance', req, httpOptions)
      .toPromise()
      .then(response => response)
      .catch(this.handleError);
  }

  private handleError(error): Promise<any> {
    console.log('Error From Service: ' + error);
    return Promise.reject(error.message || error);
  }

  ngOnInit() {
    this.aadhaarNo = this.sessionManagerService.getAadhaar();
    this.userType = this.sessionManagerService.getUserType();
    this.serviceService.getTransactions(this.aadhaarNo, this.userType).then((res: any) => {
      this.resArr = res.result;
      return res.result;
    });
  }
}
