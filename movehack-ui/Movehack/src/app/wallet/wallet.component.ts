import { Component, OnInit } from '@angular/core';
import { WindowRefService } from "../WindowRef/window-ref.service";
import { WalletService } from "./wallet.service";
import { SessionManagerService } from "../SessionManager/session-manager.service";
import { Location } from "@angular/common";
import { Router } from '@angular/router';
import { ServiceService } from '../service/service.service';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.css']
})
export class WalletComponent implements OnInit {

  amount: string;
  walletBalance: string;
  aadhaarNo: string;
  qrData: string;

  mobile: String;
  firstName: string;
  lastName: string;
  email: string;
  transactionDetails: any;
  
  constructor(private windowRefService: WindowRefService,
    private walletService: WalletService,
    private sessionServiceManager: SessionManagerService,
    private location: Location,
    private service: ServiceService,
    private router: Router) {

    this.aadhaarNo = this.sessionServiceManager.getAadhaar();
    this.transactionDetails = [];

    this.service.getTransactions(this.aadhaarNo, "customer").then((res) => {
      this.transactionDetails = res;
      console.log(this.transactionDetails)
    });

    this.firstName = this.sessionServiceManager.getUserFirstName();
    this.lastName = this.sessionServiceManager.getUserLastName();
    this.email = this.sessionServiceManager.getUserLastName();
    this.mobile = this.sessionServiceManager.getMobile();



  }

  rzp1: any;
  paymentReq: any;
  options: any = {
    "key": "rzp_test_BPh9OrHFTRwPqt",
    "amount": "0",
    "name": "MoveHack"
  };


  public initPay(amt): void {
    amt = parseFloat(amt) * 100;
    this.options.handler = ((response) => {
      this.paymentReq = {
        "paymentId": response.razorpay_payment_id,
        "amount": amt,
        "aadhaarNo": this.aadhaarNo
      };
      this.walletService.capturePayment(this.paymentReq)
        .then((res) => {
          if (res.result == 200) {
            location.reload();
            //TODO: Show in UI
            alert("Amount Added successfully!");
          }
        });
    });

    this.options.amount = amt;
    this.rzp1 = new this.windowRefService.nativeWindow.Razorpay(this.options);
    this.rzp1.open();
  }

  private getWalletBalance() {
    this.walletService.getWalletBalance({ "aadhaarNo": this.aadhaarNo })
      .then((res) => {
        console.log(res.result);
        this.walletBalance = (parseFloat(res.result) / 100).toString();
      });

  }

  ngOnInit() {
    if (this.sessionServiceManager.getUserSessionToken() === null || this.sessionServiceManager.getUserSessionToken() === undefined) {
      this.router.navigateByUrl('/login');
    }
    this.amount = "0.00";
    this.aadhaarNo = this.sessionServiceManager.getAadhaar();

    this.getWalletBalance();
    this.qrData = this.aadhaarNo;
  }

  public convertDate(dateStr) {
    var date = new Date(dateStr);
    return date.toLocaleString();
  }
}



