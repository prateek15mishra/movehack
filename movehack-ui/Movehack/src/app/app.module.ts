
import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpHeaders } from '@angular/common/http'
import { ReactiveFormsModule } from '@angular/forms';

import { NavComponent } from './nav/nav.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';

import { WalletComponent } from './wallet/wallet.component';

import { LoginComponent } from './aadhar-login-component/login.component';
import { WindowRefService } from "./WindowRef/window-ref.service";
import { LoginService } from './aadhar-login-component/login.service';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction'   // agm-direction
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import {SignUpComponent} from './SignUpComponent/signup.component'
import {StorageServiceModule} from "ngx-webstorage-service";
import { NgxQRCodeModule } from 'ngx-qrcode2';
import {QrCodeComponent} from './QrCodeComponent/qr-code.component';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Component } from '@angular/core';
import { PayComponent } from './pay/pay.component';

import { QrScannerComponent } from './QrScannerComponent/qr-scanner.component';
import { VendorComponent} from './VendorComponent/vendor.component';
import { UserTransactionComponent } from './user-transaction/user-transaction.component';

export const httpOptions = {
  headers: new HttpHeaders({
    "X-Parse-Application-Id": "myAppId",
    "Content-Type": "application/json",
    "X-Parse-REST-API-Key": "myRestKey"
  })
};

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    NavComponent,
    HomeComponent,
    LoginComponent,
    WalletComponent,
    SignUpComponent,
    QrCodeComponent,
    QrScannerComponent,
    UserTransactionComponent,
    PayComponent,
    VendorComponent,
    UserTransactionComponent

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AngularFontAwesomeModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCLQRpy9eTdiVhK4CvjsNWrCaFNLOSBaio',
      libraries: ['places']
    }),
    AgmDirectionModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    GooglePlaceModule,
    StorageServiceModule,
    NgxQRCodeModule,
    ZXingScannerModule.forRoot(),
    RouterModule.forRoot([
      {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
      }, {
        path: 'home',
        component: HomeComponent
      }, {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'wallet',
        component: WalletComponent
      },
      {
        path: 'signup',
        component: SignUpComponent

      },{
        path: 'pay',
        component: PayComponent

      },
      {
        path: 'vendor',
        component: VendorComponent
      },{
        path: 'transaction',
        component: UserTransactionComponent
      }
    ])
  ],

  providers: [WindowRefService, LoginService],
  bootstrap: [AppComponent]
})

export class AppModule {
}

