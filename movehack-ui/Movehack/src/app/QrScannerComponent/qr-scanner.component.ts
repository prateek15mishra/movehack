import { Component,EventEmitter, VERSION, OnInit, ViewChild, Input, Output } from '@angular/core';

import { ZXingScannerComponent } from '@zxing/ngx-scanner';

import { Result } from '@zxing/library';

@Component({
    selector: 'qr-scanner',
    templateUrl: './qr-scanner.component.html',
    styleUrls: ['./qr-scanner.component.css']
})
export class QrScannerComponent implements OnInit {

    ngVersion = VERSION.full;

    @ViewChild('scanner')
    scanner: ZXingScannerComponent;

    hasCameras = false;
    hasPermission: boolean;
    qrResultString: string;
    location: any;
    availableDevices: MediaDeviceInfo[];
    selectedDevice: MediaDeviceInfo;

    @Input()
    vendorData: any = "758194450368";
    
    @Input()
    vendorType: any = "Bus";

    @Output()
    qrData = new EventEmitter();

    ngOnInit(): void {

        this.scanner.camerasFound.subscribe((devices: MediaDeviceInfo[]) => {
            this.hasCameras = true;

            console.log('Devices: ', devices);
            this.availableDevices = devices;

            // selects default camera 
            for (const device of devices) {
                this.scanner.changeDevice(device);
                this.selectedDevice = device;
                break;
            }
        });

        this.scanner.camerasNotFound.subscribe((devices: MediaDeviceInfo[]) => {
            console.error('An error has occurred when trying to enumerate your video-stream-enabled devices.');
        });

        this.scanner.permissionResponse.subscribe((answer: boolean) => {
          this.hasPermission = answer;
        });
    }

    handleQrCodeResult(resultString: string) {
        console.log('Result: ', resultString);
            this.qrResultString = resultString;

        var promise = new Promise( function(resolve,reject) {
            if ("geolocation" in navigator) {
                navigator.geolocation.getCurrentPosition((position) => {
                  resolve(position);
                });
            }
        });
        Promise.resolve(promise).then((position) => {
            var qrDataToSend = {
                "customerAadhaar":this.qrResultString,
                "location":position
            }
            this.qrData.emit(qrDataToSend);
        })
    }

    onDeviceSelectChange(selectedValue: string) {
        console.log('Selection changed: ', selectedValue);
        this.selectedDevice = this.scanner.getDeviceById(selectedValue);
    }
}