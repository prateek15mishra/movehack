export class Res{
    public amount : number;
    public from : string;
    public to:string;
    public transactionStatus: number;

    constructor(amount:number, from:string, to:string, trans: number) {
        this.amount = amount;
        this.from = from;
        this.to = to;
        this.transactionStatus = trans;
    }
}