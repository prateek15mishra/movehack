import { Component, Input, AfterViewInit, OnInit } from '@angular/core';
import { ServiceService } from '../service/service.service';
import { Res } from './Res';
import { SessionManagerService } from '../SessionManager/session-manager.service';
import { Router } from '@angular/router';

@Component({
    selector: 'vendor',
    templateUrl: './vendor.component.html'
})


export class VendorComponent implements OnInit {
    @Input()
    aadhaarNo : string = "321";

    @Input()
    vendorType: string = "Bus";
    
    openForScan: boolean;
    showAlertMessage: boolean;
    alertMessage: string;
    transactionDetails: any;
    customerAadhaar: string;

    ngOnInit() {
        
    }
    constructor(private serviceService: ServiceService, private sessionServiceManager: SessionManagerService, private route: Router) {
        this.openForScan = false;
        this.showAlertMessage = false;
        this.transactionDetails = [];
        this.serviceService.getTransactions(this.aadhaarNo, "vendor").then((res) => {
            this.transactionDetails = res;
        });
    }

    showAlert() {
        setTimeout(function () {
            this.showAlertMessage = true
        }, 2000);
    }

  sendSwipeRequest(qrData: any){
      console.log(qrData);
      this.customerAadhaar = qrData.customerAadhaar;
      if(this.openForScan) {
        this.serviceService.swipe(qrData.customerAadhaar, this.aadhaarNo, qrData.location, this.vendorType).then((response) => {
            if(response.result === 200){
                this.alertMessage = "Scanned Successfull !";
                this.showAlert()
            }
            else if(response.result === 400) {
                this.openForScan = true;
                this.alertMessage = "Insufficient funds !";
                this.showAlert();

            }
        });
        this.openForScan = false;
     }  
  }

    openAgain() {
        this.openForScan = true;
    }

    close() {
        console.log("Close")
        this.openForScan = false;
    }
}
