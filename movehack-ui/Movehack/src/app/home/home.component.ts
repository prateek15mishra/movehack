import { Component, OnInit } from '@angular/core';

import { ElementRef, NgZone, ViewChild } from '@angular/core';


import { FormGroup } from '@angular/forms';
import { MapsAPILoader } from '@agm/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { } from '@types/googlemaps';

import { ServiceService } from '../service/service.service';
import { TravelOptions, RouteForMap } from './TravelOptions';
import { SessionManagerService } from "../SessionManager/session-manager.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})

export class HomeComponent implements OnInit {


  public lat: number;
  public lng: number;
  public routeDataRequestedAndNotRecieved: boolean = false;
  public zoom: number = 16;
  public routeList: TravelOptions[] = [];
  public showMapDirections: boolean = false;

  public selectedMapRoute: RouteForMap[];
  public letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
  public origin: {};
  public destination: {};

  public originMarkerOptions: {};
  public destinationMarkerOptions: {};
  public wayPointsMarkerOptions: any = [];
  public markerOptions = {};

  @ViewChild('source') public sourceLocation: ElementRef;
  @ViewChild('destination') public destinationLocation: ElementRef;

  constructor(private mapsAPILoader: MapsAPILoader, private ngZone: NgZone, private mapuiservice: ServiceService,
    private sessionManagerService: SessionManagerService,
    private router: Router) {

      let customerType = sessionManagerService.getUserType();
      
      //console.log()
      if(customerType == 'vendor' || customerType == 'Vendor') {
        this.router.navigateByUrl("/vendor");
      }
  }

  public setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.showMapDirections = false;
        this.routeDataRequestedAndNotRecieved = false;
        console.log("Current location", position, navigator, this.lat, this.lng);

      });
    }
  }

  public renderOptions = {
    suppressMarkers: true,
  }

  public loadOnMap(route: TravelOptions) {
    this.showMapDirections = false;

    this.routeList.forEach((data) => {
      data.selected = false;
    })

    route.selected = true;

    this.selectedMapRoute = [];
    let j = 1;
    route.route.forEach((data) => {
      console.log(data)
      let origin = { 'lat': data.origin.lat, 'lng': data.origin.long };
      let destination = { 'lat': data.destination.lat, 'lng': data.destination.long };
      let icon = '', destIcon;

      /*
      if (j == 1) {
        icon = '/assets/icons/start.png';
      } else {
        if (data.mode == 'TRANSIT') {
          if (null != data.transitDetails) {
            if (data.transitDetails.vehicleType == 'BUS') {
              icon = '/assets/icons/bus.png'
            } else if (data.transitDetails.vehicleType == 'METRO') {
              icon = '/assets/icons/metro.png'
            }
          }
        } else {
          if (data.transitDetails.vehicleType == 'CAB') {
            icon = '/assets/icons/cab.png';
          } else if (data.transitDetails.vehicleType == 'SHARE') {
            icon = '/assets/icons/cab.png';
          } else if (data.transitDetails.vehicleType == 'MOTO') {
            icon = '/assets/icons/bike.jpg';
          }
        }
      }

      if (j == route.route.length) {
        //Last One
        destIcon = '/assets/icons/dest.png';
        this.markerOptions = {
          'origin': {
            'icon': icon
          },
          'destination': {
            'icon': icon,
            'label' : data.distance+'',
            'opacity' : 0.8
          }
        }
      } else {
        this.markerOptions = {
          'origin': {
            'icon': icon
          },
          'destination': {
            'icon': '/aa.png',
            'label' : data.distance+'',
            'opacity' : 0.8
          }
        }
      }
      console.info(data, icon, destIcon, j)
      */
     
      icon = '/assets/icons/start.png';
      this.markerOptions = {
        'origin': {
          'icon': icon,
          'label': this.letters[j]
        },
        'destination': {
          'icon': '/aa.png',
          'label': '',
          'opacity': 0.8
        }
      }

      this.selectedMapRoute.push(new RouteForMap(origin, destination, this.markerOptions));
      j++;
      if(j==26) {
        j =0;
      }
    });

    this.showMapDirections = true;
  }

  public findRoute() {
    this.routeDataRequestedAndNotRecieved = true;
    console.log(this.origin, this.destination)
    this.mapuiservice.getRoute(this.origin, this.destination)
      .then((dataList: TravelOptions[]) => {
        console.log("Data from server", dataList)
        this.routeList = [];

        let i: number = 0;
        dataList.forEach((data) => {
          console.log("Route", data)
          if (i == 0) {
            data.selected = true;
            this.loadOnMap(data);
          } else {
            data.selected = false;
          }
          
          this.routeList.push(data);
          i++;
        });
        this.routeDataRequestedAndNotRecieved = false;
      });

  }

  ngOnInit() {
    if (this.sessionManagerService.getUserSessionToken() === null || this.sessionManagerService.getUserSessionToken() === undefined) {
      this.router.navigateByUrl('/login');
    }

    this.setCurrentPosition();
    this.mapsAPILoader.load().then(
      () => {
        let sourceAutocomplete = new google.maps.places.Autocomplete(this.sourceLocation.nativeElement, { types: ["address"] });
        let destinationAutoComplete = new google.maps.places.Autocomplete(this.destinationLocation.nativeElement, { types: ["address"] });

        sourceAutocomplete.addListener("place_changed", () => {

          this.ngZone.run(() => {
            let place: google.maps.places.PlaceResult =
              sourceAutocomplete.getPlace();

            if (place.geometry === undefined || place.geometry === null) {
              return;
            } else {
              this.origin = { lat: place.geometry.location.lat(), lng: place.geometry.location.lng() }
            }
          });
        });

        destinationAutoComplete.addListener("place_changed", () => {
          console.log("dd")
          this.ngZone.run(() => {
            let place: google.maps.places.PlaceResult =
              destinationAutoComplete.getPlace();

            if (place.geometry === undefined || place.geometry === null) {
              return;
            } else {
              this.destination = { lat: place.geometry.location.lat(), lng: place.geometry.location.lng() }
            }
          })
        });
      }
    );
  }
}
