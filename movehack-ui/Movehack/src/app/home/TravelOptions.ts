
export class TravelOptions {

    public routeDescription: string;
    public routePreference: number;
    public comfortPercentage: number;
    public startAddress: string;
    public endAddress: string;
    public totalDistance: number; //meters
    public totalDuration: number; // seconds
    public totalFare: number //INR
    public selected?:boolean = false;
    public route: Route[];
    
    constructor(routeDescription: string, routePreference: number, comfortPercentage: number, startAddress: string, endAddress: string, totalDistance: number, totalDuration: number, totalFare: number, route: Route[]) {
        this.route = route;
        this.comfortPercentage = comfortPercentage;
        this.routeDescription = routeDescription;
        this.routePreference = routePreference;

        this.startAddress = startAddress;
        this.endAddress = endAddress;
        this.totalDistance = totalDistance;
        this.totalDuration = totalDuration;
        this.totalFare = totalFare;
    }
}
export class RouteForMap {
    public origin: {};
    public destination: {};
    public markerOptions: {};
    
    constructor(origin:{}, destination:{}, markerOptions?:{}){
        this.origin = origin;
        this.destination = destination;
        this.markerOptions = markerOptions;
    }
}

export class TransitDetail {
    public departureStop?: string;
    public departureTime?: string;
    public arrivalStop?: string;
    public arrivalTime?: string;
    public vehicleType?: string;
    public vehicleIcon?: string;

    constructor (vehicleType: string) {
        this.vehicleType = vehicleType;
    }

}

export class LatLng {
    public lat: number;
    public long: number;
    constructor(lat: number, lng: number) {
        this.lat = lat;
        this.long = lng;
    }
}

export class Route {
    public origin: LatLng;
    public destination: LatLng;
    public displayText: string;
    public mode: string;
    public travelTime: number; //seconds
    public distance: number; //meter
    public fare: number; //INR
    public transitDetails?: TransitDetail;

    constructor(origin: LatLng, destination: LatLng, displayText: string, mode: string,  travelTime: number, distance: number, fare: number, transitDetails?: TransitDetail) {
        this.origin = origin;
        this.destination = destination;
        this.displayText = displayText;
        this.mode = mode;
        this.travelTime = travelTime;
        this.distance = distance;
        this.fare = fare;
        if (undefined !== transitDetails || null != transitDetails) {
            this.transitDetails = transitDetails;
        }
    }
}