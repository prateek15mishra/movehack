export class MarkerOptions  {
    origin:MarkerInput;
    destination: MarkerInput;
}

class MarkerInput {
    public icon: string;
    public label: string;
}