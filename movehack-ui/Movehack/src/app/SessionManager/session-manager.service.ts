import {Inject, Injectable} from '@angular/core';
import {SESSION_STORAGE, StorageService} from 'ngx-webstorage-service';
import {HttpClientModule, HttpHeaders, HttpClient} from "@angular/common/http";
import {User} from "../user-transaction/UserDetails";
import {ServiceService} from "../service/service.service";

@Injectable({
  providedIn: 'root'
})
export class SessionManagerService {
  baseUri: string;

  constructor(@Inject(SESSION_STORAGE) private storage: StorageService,
              private http: HttpClient,
              private service: ServiceService) {
    this.baseUri = "http://ec2-13-232-98-71.ap-south-1.compute.amazonaws.com/parse/"
  }

  public setUserSession(sessionToken) {
    this.storage.set('userSessionToken', sessionToken);
  }

  public getGetUserDetails(sessionToken): Promise<User> {
    const httpOptions = {
      headers: new HttpHeaders({
        "X-Parse-Application-Id": "myAppId",
        "Content-Type": "application/json",
        "X-Parse-REST-API-Key": "myRestKey"
      })
    };
    var post_body = {
      "sessionToken": sessionToken
    };
    return this.http.post(this.baseUri + "functions/getUserData", post_body, httpOptions)
      .toPromise()
      .then((res:any) => {
        console.log("Service called for user object ", res.result);
        this.service.setUserObject(res.result);
        return res;
      })
      .catch(this.handleError);
  }


  public getUserSessionToken(): string {
    return this.storage.get('userSessionToken');
  }

  public getCloudUserName(sessionToken): Promise<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        "X-Parse-Application-Id": "myAppId",
        "Content-Type": "application/json",
        "X-Parse-REST-API-Key": "myRestKey"
      })
    };
    var post_body = {
      "sessionToken": sessionToken
    };
    return this.http.post(this.baseUri + "functions/getUserName", post_body, httpOptions)
      .toPromise()
      .then(res => res)
      .catch(this.handleError);
  }

  public getUserName(): string {
    return this.storage.get('userName');
  }

  public getUserSessionAadhaarNo(): string {
    return this.storage.get('userAadhaarNo');
  }

  /*
  Set & get
  */
  public setUserFirstName(firstName) {
    this.storage.set('userFirstName', firstName);
  }

  public getUserFirstName(): string {
    return this.storage.get('userFirstName');
  }

  public setUserLastName(lastName) {
    this.storage.set('userLastName', lastName);
  }

  public getUserLastName(): string {
    return this.storage.get('userLastName');
  }

  public setUserMobile(mobile) {
    this.storage.set('mobile', mobile);
  }

  public getMobile(): string {
    return this.storage.get('mobile');
  }

  public setEmail(email) {
    this.storage.set('email', email);
  }

  public getEmail(): string {
    return this.storage.get('email');
  }

  public setUserType(userType) {
    this.storage.set('userType', userType);
  }
  
  public getUserType(): string {
    return this.storage.get('userType');
  }

  public setVendorType(vendorType) {
    this.storage.set('vendorType', vendorType);
  }

  public getVendorType(): string {
    return this.storage.get('vendorType');
  }

  public setAdhar(adhaar) {
    this.storage.set('adhaar', adhaar);
  }

  public getAadhaar(): string {
    return this.storage.get('adhaar');
  }

  public unsetUserSession() {
    this.storage.remove('userSessionToken');
    this.storage.remove('userAadhaarNo');

    this.storage.remove('vendorType');
    this.storage.remove('adhaar');
    this.storage.remove('userType');
    this.storage.remove('email');
    this.storage.remove('mobile');
    this.storage.remove('userLastName');
    this.storage.remove('userFirstName');
    this.service.setUserObject(new User("","","","","","",""));
  }

  private handleError(error): Promise<any> {
    console.log('Error From Service: ' + error);
    return Promise.reject(error.message || error);
  }
}
