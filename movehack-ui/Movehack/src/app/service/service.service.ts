import { Injectable } from '@angular/core';
import { TravelOptions, Route, LatLng, TransitDetail } from '../home/TravelOptions';
import { HttpClient } from '@angular/common/http';
import { httpOptions } from '../app.module';

import { BehaviorSubject } from 'rxjs';

import { THIS_EXPR } from '../../../node_modules/@angular/compiler/src/output/output_ast';
import { Res } from '../VendorComponent/Res';
import {User} from "../user-transaction/UserDetails";


@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  private firstNameSubs: BehaviorSubject<string> = new BehaviorSubject("");
  public firstName =  this.firstNameSubs.asObservable();

  private lastNameSubs: BehaviorSubject<string> = new BehaviorSubject("");
  public lastName =  this.lastNameSubs.asObservable();

  private mobileSubs: BehaviorSubject<string> = new BehaviorSubject("");
  public mobile =  this.mobileSubs.asObservable();

  private aadhaarSub: BehaviorSubject<string> = new BehaviorSubject("");
  public aadhaar =  this.aadhaarSub.asObservable();

  private emailSubs: BehaviorSubject<string> = new BehaviorSubject("");
  public email =  this.emailSubs.asObservable();

  private vendorTypSubs: BehaviorSubject<string> = new BehaviorSubject("");
  public vendorType=  this.vendorTypSubs.asObservable();

  private userTypeSubs: BehaviorSubject<string> = new BehaviorSubject("");
  public userType=  this.userTypeSubs.asObservable();


  public setUserObject(user: User){
    console.log("User from service service" , user);

    this.firstNameSubs.next(user.fname);
    this.lastNameSubs.next(user.lname);
    this.aadhaarSub.next(user.aadhaar);
    this.emailSubs.next(user.email);
    this.mobileSubs.next(user.phoneNo);
    this.userTypeSubs.next(user.userType);
    this.vendorTypSubs.next(user.vendorType);

    console.log("Called next")
  }


  private loggedInVar: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public logMsg = this.loggedInVar.asObservable();

  public setLoggedIn(value: boolean) {
    this.loggedInVar.next(value);
  }
  

  baseUri : string = "http://ec2-13-232-98-71.ap-south-1.compute.amazonaws.com/parse/functions";


  constructor(private http: HttpClient) { }
  /*
    {
      "origin": {
          "lat": 17.4944085,
          "long": 78.3158931
      },
      "destination": {
          "lat": 17.4950245,
          "long": 78.3158706
      },
      "mode": "WALKING",
      "time": 71
  },
  */
  public getRoute(originLatLong, destinationLatLong): Promise<TravelOptions[]> {

    /*let travelOpts: TravelOptions[] = [];
    let route :Route[] = [];
    route.push(new Route(new LatLng(17.4175, 78.3463),new LatLng(17.2403, 78.4294), "Direct Cab Till RGIA", "CAB", 3600, 39800,600));
    
    travelOpts.push(
      new TravelOptions(
        "Best Cab Route",
        1,
        89,
        "Waverock",
        "RGIA",
        39800,
        3600,
        750,
        route  
      ));

    route = [];
    route.push(new Route(new LatLng(17.4175, 78.3463),new LatLng(17.4354, 78.3407), "Auto Till ISB Bus Stand", "TRANSIT",  2000, 1200,50, new TransitDetail('AUTO')));
    route.push(new Route(new LatLng(17.4354, 78.3407),new LatLng(17.4401, 78.3489), "Bus 18A towards ORR Circle", "TRANSIT",  5000, 1500,20,new TransitDetail('BUS')));
    route.push(new Route(new LatLng(17.4401, 78.3489),new LatLng(17.2403, 78.4294), "PushPak BusTowards ORR", "TRANSIT", 32800, 3600,220,new TransitDetail('CAB')));
    
    travelOpts.push(
      new TravelOptions(
        "Optimized Cost Saving Option",
        2,
        78,
        "Waverock",
        "RGIA",
        39800,
        10600,
        270,
        route  
      ));

    return Promise.resolve(travelOpts);
  
    console.log("Service", originLatLong, destinationLatLong)
    
      var obj = {
      "origin": {
             "lat": originLatLong.lat,
             "long": originLatLong.lng
       },
      "destination": {
             "lat": destinationLatLong.lat,
             "long": destinationLatLong.lng
       }
      }; */
 var obj = {
      "origin": {
        "lat": originLatLong.lat,
        "long": originLatLong.lng
      },
      "destination": {
        "lat": destinationLatLong.lat,
        "long": destinationLatLong.lng
      }
    };

    return this.http
      .post(this.baseUri+ '/getRoutes', obj, httpOptions)
      .toPromise()
      .then((response:any) => response.result)
      .catch(this.handleError);
  }

  public getTransactions(aadhaarNo, userType) : Promise<Res[]> {
    var postData = {
      "aadhaarNo" : aadhaarNo,
      "userType" : userType
    }
    return this.http
            .post(this.baseUri+"/getTransaction", postData, httpOptions)
            .toPromise()
            .then((response:any) => response.result)
            .catch(this.handleError);
  }

  public swipe(customerAadhaar, vendorAadhaar, location, vendorType): Promise<any> {
      var postData = {
        "customerAadhaar": customerAadhaar,
        "vendorAadhaar": vendorAadhaar,
        "location": {
          "lat": location.coords.latitude,
          "lng": location.coords.longitude
        },
        "vendorType":vendorType
      }
      return this.http
                .post(this.baseUri+'/swipeInOrOut', postData, httpOptions)
                .toPromise()
                .then(response => response)
                .catch(this.handleError);
    }

  public handleError(error): Promise<any> {
    console.log('Error From Service: ' + error);
    return Promise.reject(error.message || error);
  }
}
