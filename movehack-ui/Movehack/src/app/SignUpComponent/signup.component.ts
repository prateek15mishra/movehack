import {Component, OnInit} from '@angular/core';
import {LoginService} from '../aadhar-login-component/login.service';
import {Router} from '@angular/router'
import {SessionManagerService} from "../SessionManager/session-manager.service";

@Component({
  selector: 'signup-component',
  templateUrl: './signup.component.html',
  styleUrls: ['../aadhar-login-component/login.component.css']
})
export class SignUpComponent implements OnInit {

  aadhar: string;
  phoneNo: string;
  firstName: string;
  lastName: string;
  email: string;
  userType: string;
  vendorType: string;

  options = [
    {name: "Customer", value: 1},
    {name: "Vendor", value: 2}
  ];

  vendorOptions = [
    {name: "UberGo", value: 1},
    {name: "UberAuto", value: 2},
    {name: "Moto", value: 3},
    {name: "Bus", value: 4},
    {name: "Subway", value: 5}
  ];

  constructor(private loginService: LoginService,
              private router: Router,
              private sessionManager: SessionManagerService) {
  }

  signUp() {
    let message = "Please enter the following mandatory fields {"; 
    let flag:boolean = false;

    if(null == this.firstName || this.firstName.length == 0) {
      message = message + " firstname, ";
      flag = true;
    }
    if(null == this.lastName || this.lastName.length == 0) {
      message = message + " lastname, ";
      flag = true;
    } 
    
    if(null == this.aadhar || this.aadhar.length == 0) {
      message = message + " aadhaar, ";
      flag = true;
    } 
    
    if(null == this.email || this.email.length == 0) {
      message = message + " email, ";
      flag = true;
    } 

    if(null == this.phoneNo || this.phoneNo.toString().length == 0) {
      message = message + " mobile, ";
      flag = true;
    } 
    
    if(null == this.userType || this.userType.length == 0) {
      message = message + " usertype ";
      flag = true;
    }
  

    if(null != this.userType && this.userType == 'Vendor') {
      if(null == this.vendorType || this.vendorType.length == 0) {
        message = message + " ,vendorType ";
        flag = true;
      }
    }
    
    if(flag) {
      message = message + " } ";
      alert(message);
      return;
    } else {

      let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(! re.test(this.email) ) {
        alert('Please enter a valid email address.');
        return;
      }

      if(this.phoneNo.toString().length != 10) {

        alert('Please enter a valid 10 digit mobile number. (' + this.phoneNo.toString().length + ')'); 
        return;
      }

      if(this.aadhar.toString().length != 12) {
        alert('Please enter a valid 12 digit aadhaar number'); 
        return;
      }
    }

    if(this.vendorType == null || this.vendorType.length == 0) {
      this.vendorType = "N.A.";
    }
    this.loginService.signup(this.firstName, this.lastName, this.aadhar.toString(), this.phoneNo.toString(), this.email, this.userType, this.vendorType).then(res => {
      if (res.result === 400) {
        //TODO: Show in UI
        alert("User with AADHAAR and email provided, already exists.");
      } else {
        this.sessionManager.setUserSession(res.result);
        alert('SignUp Successful! Please login now.')
        this.router.navigateByUrl('/login');
      }
    });
  }

  ngOnInit() {
  }

}
