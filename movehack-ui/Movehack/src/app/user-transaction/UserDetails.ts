import {a} from "@angular/core/src/render3";

export class User {
  public fname: string;
  public lname: string;
  public phoneNo: string;
  public email: string;
  public aadhaar: string;
  public userType: string;
  public vendorType: string;

  constructor(firstName: string, lastName: string, mobile: string, email: string, aadhar: string, userType: string, vendorType: string) {
    this.fname = firstName;
    this.lname = lastName;
    this.phoneNo = mobile;
    this.email = email;
    this.aadhaar = aadhar;
    this.userType = userType;
    this.vendorType = vendorType;
  }
}
