import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service/service.service';
import { SessionManagerService } from '../SessionManager/session-manager.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-transaction',
  templateUrl: './user-transaction.component.html',
  styleUrls: ['./user-transaction.component.css']
})
export class UserTransactionComponent implements OnInit {
  transactionDetails:any;

  constructor(private serviceService: ServiceService, private sessionManageService: SessionManagerService, private router: Router) {
    let aadhaarNo = this.sessionManageService.getAadhaar();
    console.log("Adhar", aadhaarNo)
    if(undefined != aadhaarNo && aadhaarNo.length > 0 ) {
      
      this.serviceService.getTransactions(aadhaarNo, "customer").then((res) => {
        this.transactionDetails = res;
      });

    } else {
      // alert('Session not found');
      //this.router.navigateByUrl('/login');
    }
    
  }

  ngOnInit() {
  }



}
