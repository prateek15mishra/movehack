require('./abhigyan');
require('./suman');
require('./map');
require('./weather');
require('./comfortScorer');
require('./vendor');

Parse.Cloud.afterSave(Parse.User, function (req) {
    if (!req.object.existed()) {
        var aadhaarNo = req.object.get('username');
        var Wallet = Parse.Object.extend('Wallet');
        var walletObj = new Wallet();
        walletObj.set('aadhaarNo', aadhaarNo);
        walletObj.set('amount', 0);
        walletObj.save(null, {useMasterKey: true}).then(function () {
            console.log("New Wallet created for aadhaar : ", aadhaarNo);
        })
    }
});

Parse.Cloud.define('getUserData', function (req, res) {
    var sessionToken = req.params.sessionToken;
    Parse.User.enableUnsafeCurrentUser();
    Parse.User.become(sessionToken, {useMasterKey: true}).then(function (user) {
        var aadhaar = user.get("username");
        var fname = user.get("fname");
        var lname = user.get("lname");
        var phoneNo = user.get("PhoneNumber");
        var email = user.get("email");
        var vendorType = user.get("vendorType");
        var userType = user.get("userType");
        res.success({
            "aadhaar": aadhaar,
            "fname": fname,
            "lname": lname,
            "phoneNo": phoneNo,
            "email": email,
            "vendorType": vendorType,
            "userType": userType
        });
    }, function (error) {
        res.error(error);
    });
});

Parse.Cloud.beforeSave('Transaction', function (req, res) {
    var vendorType = "";
    var distance = "";
    var traveltime = "";
    var fare = "";
    if (req.object.existed()) {
        const vendorAadhaar = req.object.get('vendorAadhaar');
        const customerInLocation = JSON.parse(req.object.get('customerInLocation'));
        const customerOutLocation = JSON.parse(req.object.get('customerOutLocation'));
        const Vendor = Parse.Object.extend(Parse.User);
        const vendorQuery = new Parse.Query(Vendor);
        vendorQuery.equalTo('username', vendorAadhaar);
        vendorQuery.first({ useMasterKey: true }).then(function (vendor) {
            vendorType = vendor.get('vendorType');
            var reqObj = {
                "origin": {
                    "lat": customerInLocation.lat,
                    "long": customerInLocation.lng
                },
                "destination": {
                    "lat": customerOutLocation.lat,
                    "long": customerOutLocation.lng
                },
                "mode" : vendorType
            };
            console.log(reqObj);
            Parse.Cloud.run('calculateFare', reqObj, { useMasterKey: true }).then(function(response) {
                distance = response.distance;
                traveltime = response.travelTime;
                fare = response.fare;
                req.object.set('distance', distance);
                req.object.set('travelTime', traveltime);
                req.object.set('fare', fare);
                res.success();
            })
        })
    } else {
        res.success();
    }
});
