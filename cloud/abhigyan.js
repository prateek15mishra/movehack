var Razorpay = require('razorpay');

var RazorpayInstance = new Razorpay({
    key_id: 'rzp_test_BPh9OrHFTRwPqt',
    key_secret: 'ggNGBnsiFCjIuZFQAJJFQbd9'
});

Parse.Cloud.define('capturePayment', function (req, res) {
    var paymentId = req.params.paymentId;
    var amount = req.params.amount;
    var aadhaarNo = req.params.aadhaarNo;
    RazorpayInstance.payments.capture(paymentId, amount).then(function (response) {
        var Wallet = Parse.Object.extend("Wallet");
        var query = new Parse.Query(Wallet);
        query.equalTo('aadhaarNo', aadhaarNo);
        query.first({useMasterKey: true}).then(function (foundWallet) {
            const oldAmt = foundWallet.get('amount');
            const updatedAmt = parseFloat(amount) + parseFloat(oldAmt);
            foundWallet.set('amount', updatedAmt);
            foundWallet.save(null, {useMasterKey: true}).then(function () {
                res.success(200);
            }, function (error) {
                console.log(error);
                res.error(error);
            });
        });
    }, function (error) {
        console.log(error);
        res.error();
    });
});


Parse.Cloud.define('getWalletBalance', function (req, res) {
    var aadhaarNo = req.params.aadhaarNo;
    var Wallet = Parse.Object.extend("Wallet");
    var query = new Parse.Query(Wallet);
    query.equalTo('aadhaarNo', aadhaarNo);
    query.first({useMasterKey: true}).then(function (foundWallet) {
        res.success(foundWallet.get('amount'));
    }, function (error) {
        res.error(error);
    });
});

Parse.Cloud.define('distributeBalance', function (req, res) {
    var vendors = req.params.vendors;
    var wallet = Parse.Object.extend("Wallet");
    vendors.forEach(vendor => {
        var query = new Parse.Query(wallet);
        query.equalTo('aadhaarNo', vendor.aadhaarNo);
        query.first({useMasterKey: true}).then(function (foundWallet) {
            const oldAmt = foundWallet.get('amount');
            const updatedAmt = parseFloat(vendor.amount) + parseFloat(oldAmt);
            foundWallet.set('amount', updatedAmt);
            foundWallet.save(null, {useMasterKey: true}).then(function () {
                res.success(200);
            }, function (error) {
                res.error(error);
            });
        })
    });
});

Parse.Cloud.define('debitBalance', function (req, res) {
    var aadhaarNo = req.params.aadhaarNo;
    var amount = req.params.amount;
    var Wallet = Parse.Object.extend("Wallet");
    var query = new Parse.Query(Wallet);
    query.equalTo('aadhaarNo', aadhaarNo);
    query.first({useMasterKey: true}).then(function (foundWallet) {
        const oldAmt = foundWallet.get('amount');
        const updatedAmt = parseFloat(oldAmt) - parseFloat(amount);
        foundWallet.set('amount', updatedAmt);
        foundWallet.save(null, {useMasterKey: true}).then(function () {
            res.success(200);
        }, function (error) {
            console.log(error);
            res.error(error);
        });
    });
})

Parse.Cloud.define('addBalance', function (req, res) {
    var aadhaarNo = req.params.aadhaarNo;
    var amount = req.params.amount;
    var Wallet = Parse.Object.extend("Wallet");
    var query = new Parse.Query(Wallet);
    query.equalTo('aadhaarNo', aadhaarNo);
    query.first({useMasterKey: true}).then(function (foundWallet) {
        const oldAmt = foundWallet.get('amount');
        const updatedAmt = parseFloat(oldAmt) + parseFloat(amount);
        foundWallet.set('amount', updatedAmt);
        foundWallet.save(null, {useMasterKey: true}).then(function () {
            res.success(200);
        }, function (error) {
            console.log(error);
            res.error(error);
        });
    });
})

Parse.Cloud.define('transfer', function (req, res) {
    var customerAadhaar = req.params.customerAadhaar;
    var vendorAadhaar = req.params.vendorAadhaar;
    var amount = req.params.amount;
    var customerData = {
        "aadhaarNo": customerAadhaar,
        "amount": amount
    }
    var vendorData = {
        "aadhaarNo": vendorAadhaar,
        "amount": amount
    }
    Parse.Cloud.run('debitBalance', customerData, {useMasterKey: true}).then(function (response) {
        console.log(response);
        Parse.Cloud.run('addBalance', vendorData, {useMasterKey: true}).then(function (response) {
            console.log(response);
            res.success("transfer successful");
        })
    }, function (error) {
        res.success("transaction failed");
    })
});

Parse.Cloud.define('calculateFare', function (req, res) {
    var mode = req.params.mode;
    Parse.Cloud.run('getGoogleRoutes', req, {useMasterKey: true}).then(function (response2) {
        var distance = response2["totalDistance"];
        var travelTime = response2["totalDuration"];
        if (mode !== "Subway" && mode !== "Bus") {
            Parse.Cloud.run('getUberRates', req.params, {useMasterKey: true}).then(function (response) {
                var uberResArr = response["prices"];
                console.log(uberResArr);
                for (var z in uberResArr) {
                    var uberObj = uberResArr[z];
                    var uberType = uberObj["display_name"];
                    if (uberType === mode) {
                        var uberFare = (parseFloat(uberObj["high_estimate"]) + parseFloat(uberObj["low_estimate"])) / 2;
                        res.success({
                            "fare": uberFare,
                            "distance": distance,
                            "travelTime": travelTime
                        });
                    }
                }
            });
        } else {
            var fare = 0;
            if (parseFloat(distance) <= 4000) {
                fare = 10;
            } else if (parseFloat(distance) > 4000 && parseFloat(distance) <= 12000) {
                fare = 15;
            } else if (parseFloat(distance) > 12000 && parseFloat(distance) <= 30000) {
                fare = 25;
            } else {
                fare = 30;
            }
            res.success({
                "fare": fare,
                "distance": distance,
                "travelTime": travelTime
            });
        }
    });
});