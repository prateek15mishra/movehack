// variables for sinch config
var sinchApplicationKey = "63381684-b3b0-4675-841d-b9975f198097";
var sinchApplicationSecret = "YcOYiAE6rEG1UNgWFGurHw==";
var sinchUsernameAndPassword = "application\\" + sinchApplicationKey + ":" + sinchApplicationSecret;
var sinchEncodedCredentials = new Buffer(sinchUsernameAndPassword).toString('base64');
var SinchAuth = "basic" + " " + sinchEncodedCredentials;

Parse.Cloud.define('aadhar_auth', function (req, res) {
    var aadhar = req.params.aadhar;
    var date = new Date();
    var phoneNumber = "";
    var headers = {
        "X-Parse-Application-Id": "myAppId",
        "Content-Type": "application/json",
        "X-Parse-REST-API-Key": "myRestKey"
    };

    var users = Parse.Object.extend("User");
    var query = new Parse.Query(users);
    query.equalTo('username', aadhar);
    query.first({useMasterKey: true}).then(function (user1) {
        phoneNumber = user1.get('PhoneNumber');
        var post_data = {
            "number": phoneNumber
        };
        Parse.Cloud.httpRequest({
            method: 'POST',
            headers: headers,
            url: "http://ec2-13-232-98-71.ap-south-1.compute.amazonaws.com/parse/functions/authenticate",
            body: JSON.stringify(post_data)
        }).then(function (response) {
            res.success(phoneNumber);
        })
    });
});

Parse.Cloud.define('authenticate', function (req, res) {
    var number = req.params.number;
    var date = new Date();
    var headers = {
        "Content-Type": "application/json; charset=UTF-8",
        "x-timestamp": date.toISOString(),
        "Authorization": SinchAuth
    };

    var post_data = {
        "identity": {
            "type": "number",
            "endpoint": number,
        },
        "method": "sms",
        "metadata": {
            "os": "rest",
            "platform": "N/A"
        }
    };

    Parse.Cloud.httpRequest({
        method: 'POST',
        headers: headers,
        url: "https://verificationapi-v1.sinch.com/verification/v1/verifications",
        body: JSON.stringify(post_data)
    }).then(function (response) {
        res.success("move_to_oauth");
    }, function (error) {
        res.error(error);
        console.log(error);
    });
})

Parse.Cloud.define('signup', function (req, res) {
    var user = new Parse.User();
    user.set("username", req.params.aadhar);
    user.set("password", "123");
    user.set("PhoneNumber", "+91"+req.params.phoneNumber)
    user.set("email", req.params.email);
    user.set("fname", req.params.fname);
    user.set("lname", req.params.lname);
    user.set("userType", req.params.userType);
    user.set("vendorType", req.params.vendorType);
    user.signUp(null, {useMasterKey: true}).then(function (user) {
        res.success(user.getSessionToken());
    }, function(error) {
        res.success(400);
        res.error("Error Lah! - ", error);
    });
});

Parse.Cloud.define('verify', function (req, res) {
    var number = req.params.number;
    var code = req.params.code;
    var aadhar = req.params.aadhar;
    var date = new Date();

    var headers = {
        "Content-Type": "application/json; charset=UTF-8",
        "x-timestamp": date.toISOString(),
        "Authorization": SinchAuth
    };
    var put_data = {
        "method": "sms",
        "sms": {"code": code}
    };
    Parse.Cloud.httpRequest({
        method: 'PUT',
        headers: headers,
        url: "https://verificationapi-v1.sinch.com/verification/v1/verifications/number/" + number,
        body: JSON.stringify(put_data)
    }).then(function (response) {
        if (response.status === 200) {
            Parse.User.logIn(aadhar, "123", { useMasterKey: true }).then(function(user) {
                res.success(user.getSessionToken());
            }, function(error) {
                console.log(error);
                res.error(error);
            });
        } else {
            res.success(401);
        }
    }, function (error) {
        console.log(error);
        res.success(401);
    });
});

//TODO: Suman Following Bakchodi Delete karain

Parse.Cloud.define('getCurrent', function (req, res) {
    var obj = {
        "aadhar":"758194450368"
    };
    res.success(obj);
});

Parse.Cloud.define('getDetails', function(req, res) {
    Parse.User.enableUnsafeCurrentUser();
    var currnetUser = Parse.User.current();
    res.success(currnetUser);
});

Parse.Cloud.define('authMock', function (req, res) {
    res.success("move_to_auth");
});
