var YQL = require('yql');

Parse.Cloud.define('getRainStatus', function (req, res) {
    var rainStatus = {
        "status" : false
    };
    var geoCordinates = req.params.geoCordinates;
    var woeidQueryString = 'select woeid from geo.places where text = "(' + geoCordinates.latitude + ',' + geoCordinates.longitude + ')"';
    var woeidQuery = new YQL(woeidQueryString);
    woeidQuery.exec(function (err, data) {
        const woeid = data.query.results.place.woeid;
        var queryString = 'select item.condition from weather.forecast where woeid = ' + woeid;
        var query = new YQL(queryString);
        query.exec(function (err, data) {
            var conditionCode = data.query.results.channel.item.condition.code;
            if (conditionCode >= 0 && conditionCode <= 17) {
                rainStatus.status = true;
                res.success(rainStatus);
            } else {
                rainStatus.status = false;
                res.success(rainStatus);
            }
        });
    });
});