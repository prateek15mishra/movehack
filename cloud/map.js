var baseUri = "https://maps.googleapis.com/maps/api/directions/json?";
var appKey = "AIzaSyCLQRpy9eTdiVhK4CvjsNWrCaFNLOSBaio";

Parse.Cloud.define('getRoutes', function (req, res) {
    var reqData = req.params;
    var finalResultArr = [];
    Parse.Cloud.run('getRecommendedRoute', reqData, { useMasterKey: true }).then(function (response) {
        response["routeDescription"] = "Recommended Route";
        response["routePreference"] = 2;
        finalResultArr.push(response);
        var req2 = {};
        req2.params = req;
        req.params.mode = "driving";
        Parse.Cloud.run('getGoogleRoutes', req, {useMasterKey: true}).then(function (response2) {
            Parse.Cloud.run('getUberRates', reqData, { useMasterKey: true }).then(function (response3) {
                var uberRes = response3["prices"];
                for (var i in uberRes) {
                    var uberObj = uberRes[i];
                    var uberType = uberObj["display_name"];
                    if (uberType === "UberGo") {
                        var totalFare = (parseFloat(uberObj["high_estimate"]) + parseFloat(uberObj["low_estimate"]))/2;
                        response2["totalFare"] = totalFare;
                        response2["routeDescription"] = "Best Route - Private Cab";
                        response2["routePreference"] = 1;
                        Parse.Cloud.run('getComfortRating', response2.route, {useMasterKey: true}).then(function (comfortRating) {
                            response2["comfortPercentage"] = comfortRating;
                            finalResultArr.push(response2);
                            res.success(finalResultArr);
                        });
                    }
                }
            });
        });
    });
});

Parse.Cloud.define('getRecommendedRoute', function (req, res) {
    req.params["mode"] = "transit";
    Parse.Cloud.run('getGoogleRoutes', req, {useMasterKey: true}).then(function (response) {
        var routes = response["route"];
        var thePromises = routes.map(function (x) {
            if (x["mode"] === "WALKING" && x["travelTime"] > 600) {
                var reqData = {
                    "origin": x["origin"],
                    "destination": x["destination"],
                    "distance": x["distance"]
                };
                return getUberData(reqData);
            } else {
                return x;
            }
        });
        Promise.all(thePromises).then(function (result) {
            response["route"] = result;
            response = calculateTotalFare(response);
            Parse.Cloud.run('getComfortRating', result, {useMasterKey: true}).then(function (comfortRating) {
                response["comfortPercentage"] = comfortRating;
                res.success(response);
            });
        });
    });
});

Parse.Cloud.define('getUberRates', function (req, res) {
    var origin = req.params.origin;
    var destination = req.params.destination;
    Parse.Cloud.httpRequest({
        url: 'https://api.uber.com/v1.2/estimates/price',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': 'Token vNKWZpcZul35icYygd0n6VcSIMlWAPq6Om5m0iWu'
        },
        params: {
            'start_latitude': origin.lat,
            'start_longitude': origin.long,
            'end_latitude': destination.lat,
            'end_longitude': destination.long
        }
    }).then(function (httpResponse) {
        var priceData = JSON.parse(httpResponse.text);
        res.success(priceData);
    }, function (err) {
        res.error(err);
    });
});

Parse.Cloud.define('getGoogleRoutes', function (req, res) {
    var origin = req.params.params.origin;
    var destination = req.params.params.destination;
    var mode = req.params.params.mode;
    Parse.Cloud.httpRequest({
        url: baseUri + 'origin=' + origin.lat + ',' + origin.long + '&destination=' + destination.lat + ',' + destination.long + '&mode=' + mode + '&key=' + appKey,
        method: 'GET'
    }).then(function (googleResponse) {
        var googleResp = JSON.parse(googleResponse.text);
        var googleRoutes = googleResp["routes"][0];
        if ("fare" in googleRoutes) {
            console.log("Total Fare", googleRoutes["fare"]["value"]);
        }
        var routeLeg = googleRoutes["legs"][0];
        var totalDistance = routeLeg["distance"]["value"];
        var totalDuration = routeLeg["duration"]["value"];
        var startAddress = routeLeg["start_address"];
        var endAddress = routeLeg["end_address"];
        var stepsResp = routeLeg["steps"];
        var stepArr = [];
        for (var i in stepsResp) {
            var step = stepsResp[i];
            var stepData = {
                "origin": {
                    "lat": step["start_location"]["lat"],
                    "long": step["start_location"]["lng"]
                },
                "destination": {
                    "lat": step["end_location"]["lat"],
                    "long": step["end_location"]["lng"]
                },
                "displayText": step["html_instructions"].replace(/<\/?[^>]+(>|$)/g, ""),
                "mode": step["travel_mode"],
                "travelTime": step["duration"]["value"],
                "distance": step["distance"]["value"],
                "fare": 0
            };

            var travelMode = step["travel_mode"];
            if (travelMode === "TRANSIT") {
                var fare = 0;
                if (parseFloat(stepData["distance"]) <= 4000) {
                    fare = 10;
                } else if (parseFloat(stepData["distance"]) > 4000 && parseFloat(stepData["distance"]) <= 12000) {
                    fare = 15;
                } else if (parseFloat(stepData["distance"]) > 12000 && parseFloat(stepData["distance"]) <= 30000) {
                    fare = 25;
                } else {
                    fare = 30;
                }
                stepData["fare"] = fare;
                var transitDetails = step["transit_details"];
                stepData["transitDetails"] = {
                    "departureStop": transitDetails["departure_stop"]["name"],
                    "departureTime": transitDetails["departure_time"]["text"],
                    "arrivalStop": transitDetails["arrival_stop"]["name"],
                    "arrivalTime": transitDetails["arrival_time"]["text"],
                    "vehicleType": transitDetails["line"]["vehicle"]["type"],
                    "vehicleIcon": "http:" + transitDetails["line"]["vehicle"]["icon"]
                };
            }
            stepArr.push(stepData);
        }
        var finalResponse = {
            "startAddress": startAddress,
            "endAddress": endAddress,
            "totalDistance": totalDistance,
            "totalDuration": totalDuration,
            "totalFare": 0,
            "route": stepArr
        };
        res.success(finalResponse);
    }, function (error) {
        console.log(error);
    });
});

function getUberData(uberRequestData) {
    return new Promise(function (resolve, reject) {
        Parse.Cloud.run('getUberRates', uberRequestData, {useMasterKey: true}).then(function (response) {
            var uberResArr = response["prices"];
            var respObj = {};
            var geoCordinates = {
                "geoCordinates": {
                    "latitude": uberRequestData["origin"]["lat"],
                    "longitude": uberRequestData["origin"]["long"]
                }
            };
            getWeatherCheck(geoCordinates).then(function(rainResp) {
                var rainMode = "UberAuto";
                if (rainResp.status === true) {
                    rainMode = "Moto"
                }
                for (var z in uberResArr) {
                    var uberObj = uberResArr[z];
                    var uberType = uberObj["display_name"];
                    if (uberType === rainMode) {
                        var uberFare = (parseFloat(uberObj["high_estimate"]) + parseFloat(uberObj["low_estimate"])) / 2;
                        var uberDuration = uberObj["duration"];
                        respObj["origin"] = uberRequestData["origin"];
                        respObj["destination"] = uberRequestData["destination"];
                        respObj["displayText"] = "Take " + uberType;
                        respObj["mode"] = uberType.toUpperCase();
                        respObj["travelTime"] = uberDuration;
                        respObj["fare"] = uberFare;
                        respObj["distance"] = uberRequestData["distance"];
                    }
                }
                resolve(respObj);
            });
        }, function (err) {
            reject(err);
        });
    });
}

function calculateTotalFare(req) {
    var routesArr = req["route"];
    var totalFare = 0;
    for (var i in routesArr) {
        totalFare += parseFloat(routesArr[i]["fare"]);
    }
    req["totalFare"] = totalFare;
    return req;
}

function getWeatherCheck(geoCordinates) {
    return new Promise(function (resolve, reject) {
        Parse.Cloud.run('getRainStatus', geoCordinates, {useMasterKey: true}).then(function (response) {
            resolve(response);
        }, function (err) {
            reject(err);
        });
    });
}

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}