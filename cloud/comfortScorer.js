var comfortFactor = {
    "DRIVING" : 150,
    "SUBWAY" : 90,
    "BUS" : 70,
    "MOTO" : 60,
    "UBERAUTO" : 50,
    "WALKING" : 40
};
var avgSpeedFactor = {
    "DRIVING" : 50,
    "SUBWAY" : 50,
    "BUS" : 25,
    "MOTO" : 50,
    "UBERAUTO" : 35,
    "WALKING" : 4
};

Parse.Cloud.define('getComfortRating', function (req, res) {
    var comfortArray = [];
    var reqObj = req.params;
    for (var i in reqObj) {
        var mode = reqObj[i]["mode"];
        console.log(mode);
        if (mode === "TRANSIT") {
            mode = reqObj[i]["transitDetails"]["vehicleType"];
        }
        console.log("Mode : ",mode);
        var distance = parseFloat(reqObj[i]["distance"]);
        var travelTime = parseFloat(reqObj[i]["travelTime"]);
        var speed = (distance/travelTime) * 3.6;
        console.log("Speed : " + speed);
        var avgSpeed = avgSpeedFactor[mode];
        console.log("Avg Speed that should be : ",avgSpeed);
        var speedDiff = avgSpeed - speed;
        console.log("Speed Difference : ", speedDiff);
        var cf = comfortFactor[mode];
        var x = cf - (cf * (speedDiff/avgSpeed));
        console.log("Comfort Factor : ",x);
        comfortArray.push(x);
    }
    var sum = comfortArray.reduce((a, b) => a + b, 0);
    var comfortRating = sum/comfortArray.length;
    res.success(comfortRating);
});
