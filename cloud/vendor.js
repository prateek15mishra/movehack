var async = require('async')

function checkMinimumBalance(aadhaarNo, minimumBalance) {
    return new Promise(function (resolve, reject) {
        const Wallet = Parse.Object.extend("Wallet");
        const query = new Parse.Query(Wallet);
        query.equalTo("aadhaarNo", aadhaarNo);
        query.first({useMasterKey: true}).then(function (result) {
            if (result.get("amount") < minimumBalance) {
                resolve(false);
            } else {
                resolve(true);
            }
        })
    })

}

function checkSwipeInOrOut(customerAadhaar, vendorAadhaar) {
    return new Promise(function (resolve, reject) {
        const Transaction = Parse.Object.extend("Transaction");
        const query1 = new Parse.Query(Transaction);
        query1.equalTo("customerAadhaar", customerAadhaar);
        const query2 = new Parse.Query(Transaction);
        query2.equalTo("vendorAadhaar", vendorAadhaar);
        const query = Parse.Query.and(query1, query2);
        query.find({useMasterKey: true}).then(function (results) {
            if (results.length == 0) {
                resolve(0);
            } else {
                console.log(results);
                for (var i = 0; i < results.length; i++) {
                    if (results[i].get("transactionStatus") === 1) {
                        resolve({
                            "objectId": results[i].id,
                            "customerInLocation": results[i].get("customerInLocation"),
                            "vendorType": results[i].get("vendorType")
                        });
                    }
                }
                setTimeout(function () {
                    resolve(0);
                }, 200);
            }
        })
    })
}

Parse.Cloud.define('swipeInOrOut', function (req, res) {
    var vendorAadhaar = req.params.vendorAadhaar;
    var customerAadhaar = req.params.customerAadhaar;
    var amount = 120; // to get and calculate
    var minimumBalance = 100;
    var vendorType = req.params.vendorType;
    var location = req.params.location;
    var date = new Date();
    // minimum wallet balance check
    var promise1 = checkMinimumBalance(customerAadhaar, minimumBalance);
    Promise.resolve(promise1).then(function (res1) {
        if (res1 == true) {
            // check whether swipe in or swipe out
            var promise2 = checkSwipeInOrOut(customerAadhaar, vendorAadhaar);
            Promise.resolve(promise2).then(function (swipeInOrOut) {
                if (swipeInOrOut === 0) {
                    // create a transaction entry swipe in
                    var Transaction = Parse.Object.extend("Transaction");
                    var newTransaction = new Transaction();
                    newTransaction.set("customerAadhaar", customerAadhaar);
                    newTransaction.set("vendorAadhaar", vendorAadhaar);
                    newTransaction.set("customerIn", date.toISOString());
                    newTransaction.set("transactionStatus", 1);
                    newTransaction.set("customerInLocation", JSON.stringify(location));
                    newTransaction.save(null, {useMasterKey: true}).then((newTransaction) => {
                        console.log("Started Transaction ...");
                        res.success(200);
                    }, function (error) {
                        res.error(error);
                    })
                } else {
                    // update transaction entry for swipe out
                    var Transaction = Parse.Object.extend("Transaction");
                    var query = new Parse.Query(Transaction);
                    query.get(swipeInOrOut["objectId"], {useMasterKey: true}).then(function (transaction) {
                        transaction.set("transactionStatus", 2);
                        transaction.set("customerOut", date.toISOString());
                        transaction.set("amount", amount);
                        transaction.set("transactionStatus", 2);
                        transaction.set("customerOutLocation", JSON.stringify(location));
                        transaction.set("vendorType", vendorType);
                        console.log("trying to save ....");
                        transaction.save(null, {useMasterKey: true}).then((test) => {
                            res.success(200);
                        }, function (error) {
                            res.error(error);
                        })
                    });
                }
            })
        } else {
            console.log("Unsufficient funds !")
            res.success(400);
        }
    })
})


Parse.Cloud.define('getTransaction', function (req, res) {
    var aadhaarNo = req.params.aadhaarNo;
    var userType = req.params.userType;
    var Transaction = Parse.Object.extend("Transaction");
    var query = new Parse.Query(Transaction);
    if (userType.toLowerCase() === "vendor") {
        query.equalTo("vendorAadhaar", aadhaarNo);
        query.find({useMasterKey: true}).then(function (results) {
            var promises = results.map(function (x) {
                console.log("date", x.get("customerOut"));
                var transctionObj = {
                    "from": x.get("customerAadhaar"),
                    "to": x.get("vendorAadhaar"),
                    "amount": x.get("amount"),
                    "date": x.get("customerOut"),
                    "transactionStatus": x.get("transactionStatus"),
                    "distance": x.get("distance"),
                    "travelTime": x.get("travelTime"),
                    "fare": x.get("fare"),
                    "mode":x.get("vendorType")
                }
                return transctionObj;
            })
            Promise.all(promises).then(function (results) {
                res.success(results);
            })
        })
    } else if (userType === "customer") {
        query.equalTo("customerAadhaar", aadhaarNo);
        query.find({useMasterKey: true}).then(function (results) {
            var promises = results.map(function (x) {
                console.log("date", x.get("customerOut"));
                var transctionObj = {
                    "from": x.get("customerAadhaar"),
                    "to": x.get("vendorAadhaar"),
                    "amount": x.get("amount"),
                    "date": x.get("customerOut"),
                    "transactionStatus": x.get("transactionStatus"),
                    "distance": x.get("distance"),
                    "travelTime": x.get("travelTime"),
                    "fare": x.get("fare"),
                    "mode":x.get("vendorType")
                }
                return transctionObj;
            })
            Promise.all(promises).then(function (results) {
                res.success(results);
            })
        })
    } else {
        res.error("Invalid user type !");
    }
})
